// preloader

setTimeout(function() {
    $('#loading').addClass('hidden');
}, 1000);

// sliders

var swiperOfferPic = new Swiper('#offerSlider', {
    direction: 'vertical',
    autoplay: {
        delay: 5000,
    },
    loop: true,
    speed: 1000,
});

var swiperOfferInfo = new Swiper('#offerTable', {
    direction: 'vertical',
    autoplay: {
        delay: 5000,
    },
    loop: true,
    speed: 1000,
});

var swiperQuestion = new Swiper('#productQuestionSlider', {
    pagination: {
      el: '.swiper-pagination',
      clickable: true,
      renderBullet: function (index, className) {
        return '<span class="' + className + '">' + (index + 1) + '</span>';
      },
    },
    observer: true,
    observeParents: true,
});

var productGalleryThumbs = new Swiper('#productGalleryThumb', {
    spaceBetween: 10,
    slidesPerView: 4,
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
    observer: true,
    observeParents: true,
});

var productGalleryTop = new Swiper('#productGalleryTop', {
    spaceBetween: 10,
    thumbs: {
      swiper: productGalleryThumbs
    },
    observer: true,
    observeParents: true,
});

var picSwiper = new Swiper('#picSlider', {
  loop: true,
  centeredSlides: true,
  spaceBetween: -330,
});

var textSwiper = new Swiper('#textSlider', {
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
  loop: true,
});

textSwiper.controller.control = picSwiper;
picSwiper.controller.control = textSwiper;

// responsive header
document.querySelector('.navbar-toggler').addEventListener('click', function() {
  document.querySelector('.navbar').style.background = '#ffffff';
  document.querySelector('.navbar-toggler i').style.color = '#FF8600';
  document.querySelector('.cart-nav-close').style.display = 'none';
  document.querySelector('.cart-nav-open').style.display = 'block';
})

// product page tab content
function openTab(evt, tabId) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("product-tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("product-tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" product-tablinks--active", "");
  }
  document.getElementById(tabId).style.display = "block";
  evt.currentTarget.className += " product-tablinks--active";
}

// profile sidebar
document.querySelector('.profile-sidebar-collapse').addEventListener('click', function(){
  document.querySelector('.profile-sidebar').style.width = '300px';

  var openSidebar = document.querySelectorAll('.open-sidebar');
  openSidebar.forEach(function(openSidebar){
    openSidebar.style.display = 'block';
  })

  var closeSidebar = document.querySelectorAll('.close-sidebar');
  closeSidebar.forEach(function(closeSidebar){
    closeSidebar.style.display = 'none';
  })
})

document.getElementById('closeSidebar').addEventListener('click', function(){
  document.querySelector('.profile-sidebar').style.width = '85px';

  var openSidebar = document.querySelectorAll('.open-sidebar');
  openSidebar.forEach(function(openSidebar){
    openSidebar.style.display = 'none';
  })

  var closeSidebar = document.querySelectorAll('.close-sidebar');
  closeSidebar.forEach(function(closeSidebar){
    closeSidebar.style.display = 'block';
  })
})

// profile tab content
function opeenPanel(evt, tabsId) {
  var i, tabcontents, tablinks;
  tabcontent = document.getElementsByClassName("panel-tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    console.log(tabcontent.length)
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("panel-tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" panel-tablinks--active", "");
  }
  document.getElementById(tabsId).style.display = "block";
  evt.currentTarget.className += " panel-tablinks--active";
}

// profile stepper

$(document).ready(function () {
  
  //validation
 $('input, select').tooltipster({
     trigger: 'custom',
     onlyOne: false,
     position: 'right',
     theme: 'tooltipster-light'
   });

    $("#form").validate({
        errorPlacement: function (error, element) {
            var lastError = $(element).data('lastError'),
                newError = $(error).text();

            $(element).data('lastError', newError);

            if(newError !== '' && newError !== lastError){
                $(element).tooltipster('content', newError);
                $(element).tooltipster('show');
            }
        },
        success: function (label, element) {
            $(element).tooltipster('hide');
        }
    });


/* This code handles all of the navigation stuff.
** Probably leave it. Credit to https://bootsnipp.com/snippets/featured/form-wizard-and-validation
*/
var navListItems = $('div.setup-panel div a'),
        allWells = $('.setup-content'),
        allNextBtn = $('.nextBtn');

allWells.hide();

navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this).attr('href')),
        $item = $(this);

    if (!$item.hasClass('disabled')) {
        navListItems.removeClass('btn-primary').addClass('btn-default');
        $item.addClass('btn-primary');
        $('input, select').tooltipster("hide");
        allWells.hide();
        $target.show();
        $target.find('input:eq(0)').focus();
    }
});

/* Handles validating using jQuery validate.
*/
allNextBtn.click(function(){
    var curStep = $(this).closest(".setup-content"),
        curStepBtn = curStep.attr("id"),
        nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
        curInputs = curStep.find("input"),
        isValid = true;

    //Loop through all inputs in this form group and validate them.
    for(var i=0; i<curInputs.length; i++){
        if (!$(curInputs[i]).valid()){
            isValid = false;
        }
    }

    if (isValid){
        //Progress to the next page.
      nextStepWizard.removeClass('disabled').trigger('click');    
        // # # # AJAX REQUEST HERE # # # 
        
        /*
        Theoretically, in order to preserve the state of the form should the worst happen, we could use an ajax call that would look something like this:
        
        //Prepare the key-val pairs like a normal post request.
        var fields = {};
        for(var i= 0; i < curInputs.length; i++){
          fields[$(curInputs[i]).attr("name")] = $(curInputs[i]).attr("name").val();
        }
        
        $.post(
            "location.php",
            fields,
            function(data){
              //Silent success handler.
            }                
        );
        
        //The FINAL button on last page should have its own logic to finalize the enrolment.
        */
    }
});

$('div.setup-panel div a.btn-primary').trigger('click');

});
